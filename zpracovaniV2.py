import csv
import math

print("Start")
# delkaKroku urcuje pocet hodnot k prumerovani
delkaKroku = 10000

#---------------------------VSECHNY HODNOTY NACTU DO POLI---------------------------
pocet_radku = 0
humidity_array = []
temperature_array = []
pressure_array = []
light_array = []
magX_array = []
magY_array = []
magZ_array = []
dateTime_array = []
latitude_array = []
longtitude_array = []

for x in range(0, 9): # zvolit spravne umisteni souboru a spravny pocet souboru !
    cesta="./dataFolder/data" + str(x) + "_intergalactic_team.csv"
    cisloRadkuAktualnihoSouboru = 0
    with open(cesta) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=",")

        for row in csv_reader: # projde vsechny radky
            if (cisloRadkuAktualnihoSouboru == 0 and x == 0): # prvni radek prvniho souboru
                pocet_radku = pocet_radku + 1
                cisloRadkuAktualnihoSouboru = cisloRadkuAktualnihoSouboru + 1

            elif (cisloRadkuAktualnihoSouboru == 0 and x != 0): # prvni radek ostatnich souboru
                cisloRadkuAktualnihoSouboru = cisloRadkuAktualnihoSouboru + 1
                continue

            else: # radky, ktere nejsou prvni
                pocet_radku = pocet_radku + 1
                cisloRadkuAktualnihoSouboru = cisloRadkuAktualnihoSouboru + 1

                dateTime_array.append(row[0])
                humidity_array.append(float(row[1]))
                temperature_array.append(float(row[2]))
                pressure_array.append(float(row[3]))
                light_array.append(int(row[4]))
                magX_array.append(float(row[5]))
                magY_array.append(float(row[6]))
                magZ_array.append(float(row[7]))
                latitude_array.append(row[8])
                longtitude_array.append(row[9])

print("Pocet vstupnich dat: ", len(humidity_array))

#--------------------ZPRUMEROVANE HODNOTY HUMIDITY NACTU DO POLE--------------------

cisloRadku = 0
humidityPrumer_array = []

while cisloRadku <= pocet_radku - 1 - delkaKroku: # TODO: !!! NEOSETRUJI DELKU POLI !!!
    soucetHodnot = 0
    for x in range (cisloRadku, cisloRadku + delkaKroku):
        #nactu pole s poradim "cisloRadku" a pet nasledujicich a sectu je dohromady
        soucetHodnot = soucetHodnot + humidity_array[x]
    #vydelim hromadny soucet delkouKroku
    humidityPrumer_array.append(soucetHodnot/delkaKroku)       
    #prictu k cisluRadku delku kroku
    cisloRadku = cisloRadku + delkaKroku
    #pak se vratim na zacatek while cyklu

#------------------ZPRUMEROVANE HODNOTY TEMPERATURE NACTU DO POLE------------------

cisloRadku = 0
temperaturePrumer_array = []

while cisloRadku <= pocet_radku - 1 - delkaKroku:
    soucetHodnot = 0
    for x in range (cisloRadku, cisloRadku + delkaKroku):
        soucetHodnot = soucetHodnot + temperature_array[x]
    temperaturePrumer_array.append(soucetHodnot/delkaKroku)       
    cisloRadku = cisloRadku + delkaKroku

#------------------ZPRUMEROVANE HODNOTY PRESSURE NACTU DO POLE------------------

cisloRadku = 0
pressurePrumer_array = []

while cisloRadku <= pocet_radku - 1 - delkaKroku:
    soucetHodnot = 0
    for x in range (cisloRadku, cisloRadku + delkaKroku):
        soucetHodnot = soucetHodnot + pressure_array[x]
    pressurePrumer_array.append(soucetHodnot/delkaKroku)       
    cisloRadku = cisloRadku + delkaKroku


#----------------------NACTU PRVNI HODNOTU DATA A CASU DO POLE----------------------

cisloRadku = 0
dateTimePrumer_array = []

while cisloRadku <= pocet_radku - 1 - delkaKroku:
    #nactu do pole DateTimePrumer_array hodnotu DateTime_array z radku "cisloRadku"
    dateTimePrumer_array.append(dateTime_array[cisloRadku])
    cisloRadku = cisloRadku + delkaKroku

#-------------------------NACTU PRVNI HODNOTU LIGHT DO POLE-------------------------

cisloRadku = 0
lightPrumer_array = []

while cisloRadku <= pocet_radku - 1 - delkaKroku:
    lightPrumer_array.append(light_array[cisloRadku])
    cisloRadku = cisloRadku + delkaKroku

#-----------------------NACTU PRVNI HODNOTU LATITUDE DO POLE-----------------------

cisloRadku = 0
latitudePrumer_array = []

while cisloRadku <= pocet_radku - 1 - delkaKroku:
    latitudePrumer_array.append(latitude_array[cisloRadku])
    cisloRadku = cisloRadku + delkaKroku

#----------------------NACTU PRVNI HODNOTU LONGTITUDE DO POLE----------------------

cisloRadku = 0
longtitudePrumer_array = []

while cisloRadku <= pocet_radku - 1 - delkaKroku:
    longtitudePrumer_array.append(longtitude_array[cisloRadku])
    cisloRadku = cisloRadku + delkaKroku


#--------------------ZPRUMEROVANE HODNOTY MAGX NACTU DO POLE--------------------

cisloRadku = 0
magXPrumer_array = []

while cisloRadku <= pocet_radku - 1 - delkaKroku:
    soucetHodnot = 0
    for x in range (cisloRadku, cisloRadku + delkaKroku):
        soucetHodnot = soucetHodnot + magX_array[x]
    magXPrumer_array.append(soucetHodnot/delkaKroku)       
    cisloRadku = cisloRadku + delkaKroku


#--------------------ZPRUMEROVANE HODNOTY MAGY NACTU DO POLE--------------------

cisloRadku = 0
magYPrumer_array = []

while cisloRadku <= pocet_radku - 1 - delkaKroku:
    soucetHodnot = 0
    for x in range (cisloRadku, cisloRadku + delkaKroku):
        soucetHodnot = soucetHodnot + magY_array[x]
    magYPrumer_array.append(soucetHodnot/delkaKroku)       
    cisloRadku = cisloRadku + delkaKroku

#--------------------ZPRUMEROVANE HODNOTY MAGZ NACTU DO POLE--------------------

cisloRadku = 0
magZPrumer_array = []

while cisloRadku <= pocet_radku - 1 - delkaKroku:
    soucetHodnot = 0
    for x in range (cisloRadku, cisloRadku + delkaKroku):
        soucetHodnot = soucetHodnot + magZ_array[x]
    magZPrumer_array.append(soucetHodnot/delkaKroku)       
    cisloRadku = cisloRadku + delkaKroku

#--------------VYPOCET VELIKOSTI VEKTORU Z PRUMERU MAGX MAGY MAGZ--------------

magPrumer_array = []

for x in range (0, len(magXPrumer_array)):
    magPrumer_array.append(math.sqrt((magXPrumer_array[x]*magXPrumer_array[x])+(magYPrumer_array[x]*magYPrumer_array[x])+(magZPrumer_array[x]*magZPrumer_array[x])))

print("Pocet vystupnich dat: ", len(magPrumer_array))

#---------------------------ZAPIS DAT DO CSV SOUBORU---------------------------

with open("dataFinal_intergalactic_team.csv", mode="w", newline="") as finalFile:
    final_writer = csv.writer(finalFile, delimiter=",")

    final_writer.writerow(['Date/Time', 'Humidity', 'Temperature', 'Pressure', 'Light', 'MagAvg', 'Latitude', 'Longtitude'])

    for x in range(0, len(dateTimePrumer_array)):
        final_writer.writerow([dateTimePrumer_array[x], humidityPrumer_array[x], temperaturePrumer_array[x], pressurePrumer_array[x], lightPrumer_array[x], magPrumer_array[x], latitudePrumer_array[x], longtitudePrumer_array[x]])
    

print("Done")