from sense_hat import SenseHat
from logzero import logger
from ephem import readtle, degree
from picamera import PiCamera
import picamera.array
import numpy as np
from datetime import datetime, timedelta
import random
import os
import csv


dir_path = os.path.dirname(os.path.realpath(__file__))

# Connect to the Sense Hat
sh = SenseHat()

# clear what´s displayed on LED Matrix
sh.clear()

# Latest TLE data for ISS location
name = "ISS (ZARYA)"
l1 = "1 25544U 98067A   21048.35533911  .00000433  00000-0  16033-4 0  9992"
l2 = "2 25544  51.6430 215.4241 0002660  26.4250  48.5824 15.48966356270028"
iss = readtle(name, l1, l2)

# Set up camera for light sensing in 64x64 pixels and set up exposure and auto white balance
cam = PiCamera()
cam.resolution = (64,64)
cam.exposure_mode = 'auto'
cam.awb_mode = 'auto'

# Variables used for file handling
f = 0
writer = 0
filenum = 0
file_end_time = 0

def create_csv_file():
    '''
    This function creates new csv file and opens it for writing for the whole program
    1. creates new unique file
    2. opens it and creates global file and writer
    3. adds header
    '''
    #Define which vars are global
    global filenum
    global file_end_time
    global f
    global writer
    
    #file_end_time is used for knowing when to stop writing to a i-th file
    #and create a new (i+1)-th file
    file_end_time = datetime.now() + timedelta(minutes=60)

    #Create a new CSV file, open it and add the header row
    data_file = dir_path + "/data"+str(filenum)+"_intergal_team.csv"
    f = open(data_file, 'w')
    filenum += 1
    writer = csv.writer(f)
    header = ("Date / Time", "Humidity", "Temperature", "Pressure", "Light", "MagX", "MagY", "MagZ", "Latitude", "Longtitude", "AccX", "AccY", "AccZ")
    writer.writerow(header)
        
# function to get lat/long (from example)
def get_latlon():
    """
    A function to get lat/long of the ISS
    Example output:

    W[xxx.xxxxxxxx]
    N[xxx.xxxxxxxx]
    """
    iss.compute() # Get the lat/long values from ephem

    long_value = [float(i) for i in str(iss.sublong).split(":")]
    if long_value[0] < 0:
        long_value[0] = abs(long_value[0])
        long_value = "W" + str(long_value)
    else:
        long_value = "E" + str(long_value)

    lat_value = [float(i) for i in str(iss.sublat).split(":")]
    if lat_value[0] < 0:
        lat_value[0] = abs(lat_value[0])
        lat_value = "S" + str(lat_value)
    else:
        lat_value = "N" + str(lat_value)
    return(lat_value, long_value)


# initialise the first CSV file (data0_intergal_team.csv)
create_csv_file()

# Define a simple image and set it on LED matrix
g = [4,200,70]
o = [0,0,0]
img1 = [
g,g,g,g,g,g,g,g,
o,g,o,o,o,o,g,o,
o,o,g,o,o,g,o,o,
o,o,o,g,g,o,o,o,
o,o,o,g,g,o,o,o,
o,o,g,g,g,g,o,o,
o,g,g,g,g,g,g,o,
g,g,g,g,g,g,g,g,
]
sh.set_pixels(img1)

#Variables for changing orientation of image
#orient_index is used to iterate through the orientation array
orient_index = 0
orientation = [0,90,180,270]

#get all data before we start the main algorithm, so the first +- 150 loops we have correct data 
lat, lon = get_latlon()
temperature = round(sh.get_temperature(),6)
humidity = round(sh.get_humidity(),6)
pressure = round(sh.get_pressure(),6)

with picamera.array.PiRGBArray(cam) as stream:
    cam.capture(stream, format='rgb')
    if int(np.average(stream.array[...,1])) > 50:
        light = 1
    else:
        light = 0

#variables needed for our algorithm to do something every X iterations
loop_position = 0
loop_light = 0
loop_led = 0

#store the start time and current time
start_time = datetime.now()
now_time = start_time

#loop for 178 minutes
end_time = start_time + timedelta(minutes=178)

while (now_time < end_time):
    try:        
        #Read temp, humidity and pressure data from the Sense Hat, rounded to 6 decimal places
        temperature = round(sh.get_temperature(),6)
        humidity = round(sh.get_humidity(),6)
        pressure = round(sh.get_pressure(),6)
        
        #change the led matrix every 200 loops
        if loop_led == 199:
            # set the next rotation
            #if orient_index is out of the array (== 4), change it back to 0
            orient_index += 1
            if orient_index == 4:
                orient_index = 0
            sh.set_rotation(orientation[orient_index])           
            loop_led = 0
        else:
            loop_led += 1

        #get brightness every 150 loops
        '''
        This uses NumPy library
        It averages pixel values of a photo of the surrounding area to check if there is light in the module
        '''
        if loop_light == 149:
            with picamera.array.PiRGBArray(cam) as stream:
                cam.capture(stream, format='rgb')
                if int(np.average(stream.array[...,1])) > 50:
                    light = 1
                else:
                    light = 0
            loop_light = 0
        else:
            loop_light += 1
        
        #Read raw values of magnetic induction and split it into 3 axis
        raw = sh.get_compass_raw()
        magx = float(raw["x"])
        magy = float(raw["y"])
        magz = float(raw["z"])

        #Read raw values from accelerometer
        accelerometer_raw = sh.get_accelerometer_raw()
        accX = float(accelerometer_raw["x"])
        accY = float(accelerometer_raw["y"])
        accZ = float(accelerometer_raw["z"])

        # get latitude and longitude every 100 loops
        if loop_position == 99:
            lat, lon = get_latlon()
            loop_position = 0
        else:
            loop_position += 1

        #write data to csv file
        data = (datetime.now(), humidity, temperature, pressure, light, magx, magy, magz, lat, lon, accX, accY, accZ)
        writer.writerow(data)

        # If we need to change file, close the file we are using and create a new one
        # so we don´t have files larger than 35MB
        if now_time > file_end_time:
            f.close()
            create_csv_file()

        # update the current time
        now_time = datetime.now()

    except Exception as e:
        logger.error('{}: {})'.format(e.__class__.__name__, e))

#post main script info
data = (start_time, now_time, filenum, file_end_time)
writer.writerow(data)
sh.clear()
#politely close the file we were writing into
f.close()
#YOURS Intergal team 2020/2021
