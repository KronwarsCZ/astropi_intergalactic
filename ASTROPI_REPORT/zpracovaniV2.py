import csv
import math

print("Start")
# delkaKroku is used to know how may values we average
delkaKroku = 500

#---------------------------Load all values to their respective arrays---------------------------
pocet_radku = 0
humidity_array = []
temperature_array = []
pressure_array = []
light_array = []
magX_array = []
magY_array = []
magZ_array = []
dateTime_array = []
latitude_array = []
longtitude_array = []

for x in (0,1,2):
    cesta="data" + str(x) + "_intergal_team.csv"
    cisloRadkuAktualnihoSouboru = 0
    with open(cesta) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=",")
                
        for row in csv_reader:
            if (cisloRadkuAktualnihoSouboru == 0): # first row of a file
                cisloRadkuAktualnihoSouboru = cisloRadkuAktualnihoSouboru + 1
            
            else: # other rows with data
                pocet_radku = pocet_radku + 1
                cisloRadkuAktualnihoSouboru = cisloRadkuAktualnihoSouboru + 1

                dateTime_array.append(row[0])
                humidity_array.append(float(row[1]))
                temperature_array.append(float(row[2]))
                pressure_array.append(float(row[3]))
                light_array.append(int(row[4]))
                magX_array.append(float(row[5]))
                magY_array.append(float(row[6]))
                magZ_array.append(float(row[7]))
                latitude_array.append(row[8])
                longtitude_array.append(row[9])

print("Pocet vstupnich dat: ", len(humidity_array))

#--------------------Average humidity--------------------

cisloRadku = 0
humidityPrumer_array = []
#make average of delkaKroku values
while cisloRadku <= pocet_radku - 1 - delkaKroku:
    soucetHodnot = 0
    for x in range (cisloRadku, cisloRadku + delkaKroku):
        #make an addition of number from index cisloRadku to the next delkaKroku values
        soucetHodnot = soucetHodnot + humidity_array[x]
    humidityPrumer_array.append(soucetHodnot/delkaKroku)       
    cisloRadku = cisloRadku + delkaKroku
    #repeat

#------------------Average temperature------------------

cisloRadku = 0
temperaturePrumer_array = []

while cisloRadku <= pocet_radku - 1 - delkaKroku:
    soucetHodnot = 0
    for x in range (cisloRadku, cisloRadku + delkaKroku):
        soucetHodnot = soucetHodnot + temperature_array[x]
    temperaturePrumer_array.append(soucetHodnot/delkaKroku)       
    cisloRadku = cisloRadku + delkaKroku

#------------------Average pressure-----------------

cisloRadku = 0
pressurePrumer_array = []

while cisloRadku <= pocet_radku - 1 - delkaKroku:
    soucetHodnot = 0
    for x in range (cisloRadku, cisloRadku + delkaKroku):
        soucetHodnot = soucetHodnot + pressure_array[x]
    pressurePrumer_array.append(soucetHodnot/delkaKroku)       
    cisloRadku = cisloRadku + delkaKroku


#----------------------Average DateTime----------------------

cisloRadku = 0
dateTimePrumer_array = []

while cisloRadku <= pocet_radku - 1 - delkaKroku:
    #we just load the first value as the averaged one
    dateTimePrumer_array.append(dateTime_array[cisloRadku])
    cisloRadku = cisloRadku + delkaKroku

#-------------------------Average Light-------------------------

cisloRadku = 0
lightPrumer_array = []

while cisloRadku <= pocet_radku - 1 - delkaKroku:
    lightPrumer_array.append(light_array[cisloRadku])
    cisloRadku = cisloRadku + delkaKroku

#-----------------------Average latitude-----------------------
#same as Average Datetime
cisloRadku = 0
latitudePrumer_array = []

while cisloRadku <= pocet_radku - 1 - delkaKroku:
    latitudePrumer_array.append(latitude_array[cisloRadku])
    cisloRadku = cisloRadku + delkaKroku

#----------------------Average longtitude----------------------
#same as Average Datetime
cisloRadku = 0
longtitudePrumer_array = []

while cisloRadku <= pocet_radku - 1 - delkaKroku:
    longtitudePrumer_array.append(longtitude_array[cisloRadku])
    cisloRadku = cisloRadku + delkaKroku


#--------------------Average MagX--------------------

cisloRadku = 0
magXPrumer_array = []

while cisloRadku <= pocet_radku - 1 - delkaKroku:
    soucetHodnot = 0
    for x in range (cisloRadku, cisloRadku + delkaKroku):
        soucetHodnot = soucetHodnot + magX_array[x]
    magXPrumer_array.append(soucetHodnot/delkaKroku)       
    cisloRadku = cisloRadku + delkaKroku


#--------------------Average MagY--------------------

cisloRadku = 0
magYPrumer_array = []

while cisloRadku <= pocet_radku - 1 - delkaKroku:
    soucetHodnot = 0
    for x in range (cisloRadku, cisloRadku + delkaKroku):
        soucetHodnot = soucetHodnot + magY_array[x]
    magYPrumer_array.append(soucetHodnot/delkaKroku)       
    cisloRadku = cisloRadku + delkaKroku

#--------------------Average MagZ--------------------

cisloRadku = 0
magZPrumer_array = []

while cisloRadku <= pocet_radku - 1 - delkaKroku:
    soucetHodnot = 0
    for x in range (cisloRadku, cisloRadku + delkaKroku):
        soucetHodnot = soucetHodnot + magZ_array[x]
    magZPrumer_array.append(soucetHodnot/delkaKroku)       
    cisloRadku = cisloRadku + delkaKroku

#--------------Calculate total magnetic intensity (vector multiplication)--------------

magPrumer_array = []

for x in range (0, len(magXPrumer_array)):
    magPrumer_array.append(math.sqrt((magXPrumer_array[x]*magXPrumer_array[x])+(magYPrumer_array[x]*magYPrumer_array[x])+(magZPrumer_array[x]*magZPrumer_array[x])))

print("Pocet vystupnich dat: ", len(magPrumer_array))

#---------------------------Write data to csv file---------------------------

with open("dataFinal_intergalactic_team_KOMPLET.csv", mode="w", newline="") as finalFile:
    final_writer = csv.writer(finalFile, delimiter=",")

    final_writer.writerow(['Date/Time', 'Humidity', 'Temperature', 'Pressure', 'Light', 'MagAvg', 'Latitude', 'Longtitude'])

    for x in range(0, len(dateTimePrumer_array)):
        final_writer.writerow([dateTimePrumer_array[x], humidityPrumer_array[x], temperaturePrumer_array[x], pressurePrumer_array[x], lightPrumer_array[x], magPrumer_array[x], latitudePrumer_array[x], longtitudePrumer_array[x]])
    

print("Done")
